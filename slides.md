---
theme: apple-basic
src: ./pages/intro.md
---

---
src: ./pages/what-is-i18n.md
---

---
src: ./pages/why-matters.md
---

---
src: ./pages/status.md
---

---
src: ./pages/how-to-do-it.md
---

---
src: ./pages/common-ways.md
---

---
src: ./pages/gitlab-approach.md
---

---
src: ./pages/steps-to-accomplish.md
---

---
src: ./pages/scanning.md
---

---
src: ./pages/how-scanning-works.md
---

---
src: ./pages/what-if-raw-text-entered.md
---

---
src: ./pages/glimpse-of-process.md
---

---
src: ./pages/translation-summary.md
---

---
src: ./pages/translation-process.md
---

---
src: ./pages/back-end-ends.md
---

---
src: ./pages/frontend.md
---

---
src: ./pages/generate-js.md
---

---
src: ./pages/introduced-by.md
---

---
src: ./pages/jihu-process.md
---

---
src: ./pages/resources.md
---

---
src: ./pages/q-a.md
---