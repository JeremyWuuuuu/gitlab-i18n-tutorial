---
layout: image-right
image: 'https://images.unsplash.com/photo-1652806174998-1fc4494f19f6?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=80&raw_url=true&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=985'
image-license: Unsplash License
---

# Workflows on Crowdin

<div class="mt-10" />

<v-clicks>


i. Upload the translation source file to Crowdin.

</v-clicks>

<v-clicks>

ii. Translation gets submitted by employees/community.

</v-clicks>

<v-clicks>

iii. Crowdin proofreaders proofread the translations and approve them.

</v-clicks>

<v-clicks>

vi. Monthly downloads the approved translations and submit a MR.

</v-clicks>

<v-clicks>

v. MR gets merged and distributed to all repos.

</v-clicks>