---
layout: section
---

# GitLab's way on this

<div class="mt-20"/>

<div class="flex items-center mb-8" style="font-size: 26px;"><simple-icons-microsofttranslator class="ml-0.5 mr-3" style="font-size: 20px;" />Using a library for translating.</div>

<div class="flex items-center mb-8" style="font-size: 26px;"><fluent-window-dev-tools-24-regular class="mr-2" /> Automatically scanning the entries.</div>

<div class="flex items-center mb-8" style="font-size: 26px;"><carbon-cloud-app class="mr-2"/> Use an external tool called Crowdin for translating.</div>
