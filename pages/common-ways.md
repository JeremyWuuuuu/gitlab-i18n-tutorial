---
layout: section
---

# Common way of solving this

<div class="mt-20"/>

<div class="flex items-center mb-8" style="font-size: 26px;"><simple-icons-microsofttranslator style="font-size: 20px;" class="ml-0.5 mr-3"  />Using a library for translating.</div>

<div class="flex items-center mb-8" style="font-size: 26px;"><mdi-human-dolly class="mr-2" />Manually adding entries to a source lang file.</div>

<div class="flex items-center mb-8" style="font-size: 26px;"><mdi-human-dolly class="mr-2" />Regularly checking translation entries for updates.</div>