---
layout: section
---

# Special handling

<v-clicks>

* Jihu GitLab generates `en/app.js` for overriding some special need keys.

</v-clicks>

<v-clicks>

* Jihu GitLab imports `en/app.js` even if it is the preferred language.

</v-clicks>

