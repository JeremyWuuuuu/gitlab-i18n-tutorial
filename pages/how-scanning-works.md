---
layout: section
---

# How scanning works

By invoking this task to collect all the translation entries with these signature in files which has the extension name `*.{rb|js|vue|haml}`

<v-clicks>

* `__("Entry")`

</v-clicks>

<v-clicks>

* `s__("Entry")`

</v-clicks>

<v-clicks>

* `n__("Entry)`

</v-clicks>


<v-clicks>

GitLab CI enforces checking translations to prevent 
contributors from forgetting about updating translations.

</v-clicks>