---
layout: section
---

# Scanning

<div v-click class="text-xl p-2">

```ruby
> lib/tasks/gettext.rake

# https://github.com/grosser/gettext_i18n_rails/blob/master/lib/gettext_i18n_rails/tasks.rb

Rake::Task['gettext:find'].invoke
```

</div>
