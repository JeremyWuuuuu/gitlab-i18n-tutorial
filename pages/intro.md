---
title: GitLab i18n deep dive
layout: intro-image
image:
    /cover.jpg
---

<div class="absolute right-10 top-10">
  <h1>GitLab i18n deep dive</h1>
  <p class="text-right">Jeremy Wu</p>
</div>

<div class="absolute bottom-10 right-10">
  <p>Shot @Barcelona 2019</p>
</div>