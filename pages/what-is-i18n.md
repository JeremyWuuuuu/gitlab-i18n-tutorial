---
layout: quote
---

# "Internationalization is often written in English as i18n, where 18 is the number of letters between i and n in the English word."
W3C