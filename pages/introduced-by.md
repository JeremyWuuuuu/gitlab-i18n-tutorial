---
layout: section
---

# Introduced by

```haml
- # > app/views/layouts/header/_translations.html.haml

= javascript_include_tag locale_path unless I18n.locale == :en
- #                      ^^^^^^^^^^^        ^^^^^^^^^^^^^^^^^^
- #          locale/<language>/app.js          skip English

```
