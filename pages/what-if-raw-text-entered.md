---
layout: section
---

# Via CI checking 👮🏻

You may wonder what happens when someone insert a raw string to the application.

* For JavaScript, there is this [lint rule](https://gitlab.com/gitlab-org/frontend/eslint-plugin/-/blob/7aecb0b3d2f545c178191bd8643928c48ad87f5d/lib/rules/require-i18n-strings.js) prevent that from happening.
* For Ruby, well, there were no rules banning that yet.
