---
layout: section
---

# From PO to JSON

<v-clicks>


```ruby
# > config/initializers/gettext_rails_i18n_patch.rb

def generate_for_jed(language, overwrite = {})
    @options = parse_options(overwrite.merge(language: language))
    @parsed ||= inject_meta(parse_document)

    generated = build_json_for(build_jed_for(@parsed))
    [
      "window.translations = #{generated};"
    ].join(" ")
  end
```

</v-clicks>

<v-clicks>

Generates files under `<Rails.root>/app/locale/<language>/app.js`

</v-clicks>